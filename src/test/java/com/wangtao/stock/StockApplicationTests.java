package com.wangtao.stock;

import com.wangtao.stock.config.TestConfig;
import com.wangtao.stock.entity.DeptInfo;
import com.wangtao.stock.service.ActivityEngineService;
import com.wangtao.stock.service.HisService;
import com.wangtao.stock.service.UserService;
import com.wangtao.stock.utils.SpringBeanUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class StockApplicationTests {

    @Autowired
    HisService hisService;

    @Autowired
    UserService userService;

    @Autowired
    ActivityEngineService activityEngineService;


    @Test
    public void contextLoads() {
        List<DeptInfo> list = hisService.list();
        System.err.println(list.toString());
    }

    @Test
    public void userInfo() {
        activityEngineService.createActivityEngine();
    }

    @Test
    public void test() {

    }
}
