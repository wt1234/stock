package com.wangtao.stock.controller;

import com.wangtao.stock.dto.ArticleDTO;
import com.wangtao.stock.dto.UserDTO;
import com.wangtao.stock.entity.User;
import com.wangtao.stock.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {
    @Autowired
    UserService userService;

    @GetMapping("/params")
    public void params(String value) {
        log.info("打印错误日志");
    }

    @PostMapping("/userinfo")
    public void userinfo(@RequestBody User user) {
        userService.query();
    }

    /**
     * 简单校验
     *
     * @param articleDTO 入参
     * @return 返回值
     */
    @PostMapping("/add")
    public String add(ArticleDTO articleDTO) {
        System.err.println("aaaaaa");
        return null;
    }

    /**
     * 分组校验
     *
     * @param userDTO 入参
     */
    @PostMapping("/addValidGroup")
    public void addValidGroup(@Validated(value = UserDTO.AddUserDTO.class) UserDTO userDTO) {

    }

    /**
     * 造数
     */
    @PostMapping("/addUser")
    public void addUser() {
        userService.save();
    }
}
