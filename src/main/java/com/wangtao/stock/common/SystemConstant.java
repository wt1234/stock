package com.wangtao.stock.common;

public class SystemConstant {

    /**
     * 成功码
     */
    public static final int CODE = 200;

    /**
     * 成功消息
     */
    public static final String SUCCESS = "执行成功";

}
