package com.wangtao.stock.common;


import com.wangtao.stock.enums.ExceptionEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@SuppressWarnings("serial")
@Getter
@Setter
public class JsonResult<T> implements Serializable {

    /**
     * 响应码
     */
    private int code;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 响应消息
     */
    private String msg;

    /**
     * 总记录数
     */
    private Long total;

    public static <T> JsonResult<T> success() {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setCode(SystemConstant.CODE);
        jsonResult.setMsg(SystemConstant.SUCCESS);
        return jsonResult;
    }

    public static <T> JsonResult<T> success( T data) {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setCode(SystemConstant.CODE);
        jsonResult.setMsg(SystemConstant.SUCCESS);
        jsonResult.setData(data);
        return jsonResult;
    }

    public static <T> JsonResult<T> success( T data,long total) {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setCode(SystemConstant.CODE);
        jsonResult.setMsg(SystemConstant.SUCCESS);
        jsonResult.setData(data);
        jsonResult.setTotal(total);
        return jsonResult;
    }

    public static <T> JsonResult<T> fail(ExceptionEnum exceptionEnum){
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setMsg(exceptionEnum.getMessage());
        jsonResult.setCode(exceptionEnum.getCode());
        return jsonResult;
    }

    public static <T> JsonResult<T> fail(Integer code,String message){
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setMsg(message);
        jsonResult.setCode(code);
        return jsonResult;
    }
}
