package com.wangtao.stock.dao;

import com.wangtao.stock.entity.DeptInfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface HisDeptInfoMapper {

    @Select("select * from dept_info")
    List<DeptInfo> queryDeptInfoList();

}
