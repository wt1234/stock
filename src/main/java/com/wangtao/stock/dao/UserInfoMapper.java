package com.wangtao.stock.dao;

import com.wangtao.stock.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserInfoMapper {

    @Select("select * from user_info ")
    List<User> selectUserInfo();

    @Insert("insert into t_user(user_name,password,prize_count) values(#{userName},#{password},#{prizeCount})")
    int insertUser(User user);

}
