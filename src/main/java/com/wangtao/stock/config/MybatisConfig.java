package com.wangtao.stock.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.wangtao.stock.annotation.SwitchSource;
import com.wangtao.stock.datasource.DynamicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class MybatisConfig {

    @Autowired
    HisDataSourceConfig hisDataSourceConfig;

    /**
     * 数据源1
     *
     * @return 返回值
     * @Bean：向IOC容器中注入一个Bean
     */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean("dataSource")
    public DataSource dataSource() {
        return new DruidDataSource();
    }

    /**
     * 数据源2
     *
     * @return
     */
    @Bean(name = SwitchSource.DEFAULT_NAME)
    public DataSource hisDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(hisDataSourceConfig.getUsername());
        druidDataSource.setPassword(hisDataSourceConfig.getPassword());
        druidDataSource.setUrl(hisDataSourceConfig.getUrl());
        druidDataSource.setDriverClassName(hisDataSourceConfig.getDriverClassName());
        return druidDataSource;
    }

    /**
     * 构建一个动态数据源,用以数据源的切换
     *
     * @param dataSource    入参
     * @param hisDataSource 入参
     * @return 返回值
     */
    @Bean("dynamicDataSource")
    public DynamicDataSource dynamicDataSource(@Qualifier("dataSource") DataSource dataSource,
                                               @Qualifier("hisDataSource") DataSource hisDataSource) {
        Map<Object, Object> targetDataSources = new HashMap<>();
        //放置目标数据源
        targetDataSources.put(SwitchSource.DEFAULT_NAME, hisDataSource);
        targetDataSources.put("dataSource", dataSource);
        return new DynamicDataSource(dataSource, targetDataSources);
    }

    /**
     * 创建动态数据源SqlSessionFactory,传入的是动态数据源
     *
     * @param dataSource 入参
     * @return 返回值
     * @throws Exception 异常
     */
    @Primary
    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactoryBean(DynamicDataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        //自动将数据库中的下划线转换为驼峰格式
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setDefaultFetchSize(100);
        configuration.setDefaultStatementTimeout(30);
        sqlSessionFactoryBean.setConfiguration(configuration);
        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 重写事务管理器,管理动态数据源
     */
    @Primary
    @Bean(value = "transactionManager2")
    public PlatformTransactionManager annotationDrivenTransactionManager(DynamicDataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
