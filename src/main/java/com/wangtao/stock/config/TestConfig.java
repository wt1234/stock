package com.wangtao.stock.config;

import com.wangtao.stock.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public User getUser() {
        return new User();
    }
}
