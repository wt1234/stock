package com.wangtao.stock.config;


import com.wangtao.stock.condition.LinuxCondition;
import com.wangtao.stock.condition.WindowsCondition;
import com.wangtao.stock.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomConfig {

    @Bean("winP")
    @Conditional(value = {WindowsCondition.class})
    public User userWin() {
        return new User();
    }

    @Bean("LinuxP")
    @Conditional(value = {LinuxCondition.class})
    public User useLin() {
        return new User();
    }
}
