package com.wangtao.stock.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(value = "spring.datasource.his")
@Getter
@Setter
public class HisDataSourceConfig {

    private String driverClassName;

    private String url;

    private String username;

    private String password;

}
