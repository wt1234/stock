package com.wangtao.stock.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

//@Aspect
//@Component
public class LogAspect {

    @Pointcut("execution(* com.wangtao.stock.controller..*(..))")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void before() {
        System.err.println("AOP before");
    }

    @After("pointCut()")
    public void after() {
        System.err.println("AOP after");
    }

    @AfterReturning("pointCut()")
    public void afterReturn() {
        System.err.println("AOP afterReturn");
    }
}
