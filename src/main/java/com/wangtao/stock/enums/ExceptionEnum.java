package com.wangtao.stock.enums;

public enum ExceptionEnum {

    PARAMS_VALID_ERROR(1000,"参数校验失败");

    private int code;

    private String message;

    ExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
