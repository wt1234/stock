package com.wangtao.stock.filter;


import javax.servlet.*;
import java.io.IOException;

//@Component
public class SecondFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        System.err.println("第二个过滤器");
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
