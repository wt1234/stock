package com.wangtao.stock.filter;

import javax.servlet.*;
import java.io.IOException;

//@Component
public class CrosFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        //继续执行下一个过滤器
        System.err.println("第一个过滤器");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
