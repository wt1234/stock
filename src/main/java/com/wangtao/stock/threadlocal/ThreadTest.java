package com.wangtao.stock.threadlocal;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {

    private static InheritableThreadLocal<Integer> threadLocal = new InheritableThreadLocal<>();

    public static void test1() {
        threadLocal.set(6);
        System.err.println("父线程获取数据:" + threadLocal.get());
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        threadLocal.set(6);
        executorService.submit(() -> {
            System.err.println("第一次从线程池中获取数据:" + threadLocal.get());
        });
        threadLocal.set(7);
        executorService.submit(() -> {
            System.err.println("第二次从线程池中获取数据:" + threadLocal.get());
        });
    }

    public static void main(String[] args) {
        test1();
    }
}
