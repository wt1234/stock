package com.wangtao.stock.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeptInfo {

    private Integer id;

    private String deptName;

    private String deptCode;
}
