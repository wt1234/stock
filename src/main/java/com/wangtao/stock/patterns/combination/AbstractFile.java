package com.wangtao.stock.patterns.combination;

public abstract class AbstractFile {

    public String fileName;

    public abstract void add(AbstractFile file);

    public abstract void remove(AbstractFile abstractFile);

    public abstract AbstractFile getChild(int i);

    public abstract void killVirus();
}
