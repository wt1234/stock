package com.wangtao.stock.patterns.combination;

public class TextFile {

    private String fileName;

    public TextFile(String fileName) {
        this.fileName = fileName;
    }

    public void killVirus() {
        System.out.println("对文本文件" + fileName + "杀毒");
    }
}
