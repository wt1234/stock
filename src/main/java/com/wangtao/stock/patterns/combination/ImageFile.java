package com.wangtao.stock.patterns.combination;

public class ImageFile {

    private String fileName;

    public ImageFile(String fileName) {
        this.fileName = fileName;
    }

    public void killVirus(){
        //简化代码，模拟杀毒
        System.out.println("对图像文件"+fileName+"进行杀毒");
    }
}
