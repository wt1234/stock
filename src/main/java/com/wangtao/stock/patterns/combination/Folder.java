package com.wangtao.stock.patterns.combination;

import java.util.ArrayList;

public class Folder {

    private String name;

    //定义集合folderList,用于存储folder对象
    private ArrayList<Folder> folders = new ArrayList<>();
    //定义集合imageList,用于存储ImageFile类型的成员
    private ArrayList<ImageFile> imageFiles = new ArrayList<>();
    //定义textList,用于存储textFile类型成员
    private ArrayList<TextFile> textFiles = new ArrayList<>();

    public Folder(String name) {
        this.name = name;
    }

    public void addFolder(Folder folder) {
        folders.add(folder);
    }

    public void addImageFile(ImageFile file) {
        imageFiles.add(file);
    }

    public void addTextFile(TextFile textFile) {
        textFiles.add(textFile);
    }

    public void killVirus() {
        System.out.println("****对文件夹" + name + "进行杀毒");
        for (Folder object : folders) {
            object.killVirus();
        }

        for (ImageFile imageFile:imageFiles){
            imageFile.killVirus();
        }

        for (TextFile textFile:textFiles){
            textFile.killVirus();
        }
    }
}
