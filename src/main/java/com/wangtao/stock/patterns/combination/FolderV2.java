package com.wangtao.stock.patterns.combination;

import java.util.ArrayList;

public class FolderV2 extends AbstractFile {

    //定义集合fileList,用于存储AbstractFile成员
    private ArrayList<AbstractFile> files = new ArrayList<>();
    private String name;

    public FolderV2(String name) {
        this.name = name;
    }

    @Override
    public void add(AbstractFile file) {
        files.add(file);
    }

    @Override
    public void remove(AbstractFile abstractFile) {
        files.remove(abstractFile);
    }

    @Override
    public AbstractFile getChild(int i) {
        return files.get(i);
    }

    @Override
    public void killVirus() {
        System.out.println("对文件夹" + name + "进行杀毒");
        for (AbstractFile abstractFile:files){
            abstractFile.killVirus();
        }
    }
}
