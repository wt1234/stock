package com.wangtao.stock.patterns.builder;

public class ActorController {


    //逐步构建复杂产品对象
    public Actor builder(ActorBuilder actorBuilder) {
        Actor actor;
        actorBuilder.buildType();
        actorBuilder.buildCostume();
        actorBuilder.buildFace();
        actorBuilder.buildSex();
        actorBuilder.buildHairStyle();
        actor = actorBuilder.createActor();
        return actor;
    }
}
