package com.wangtao.stock.patterns;

import com.wangtao.stock.patterns.adapter.OperationAdapter;
import com.wangtao.stock.patterns.adapter.ScoreOperation;
import com.wangtao.stock.patterns.bridge.Image;
import com.wangtao.stock.patterns.bridge.ImageImpl;
import com.wangtao.stock.patterns.bridge.JPGImage;
import com.wangtao.stock.patterns.bridge.WindowsImpl;
import com.wangtao.stock.patterns.combination.Folder;
import com.wangtao.stock.patterns.combination.ImageFile;
import com.wangtao.stock.patterns.combination.TextFile;
import com.wangtao.stock.patterns.command.*;
import com.wangtao.stock.patterns.decorator.Component;
import com.wangtao.stock.patterns.decorator.ScrollBarDecorator;
import com.wangtao.stock.patterns.decorator.Window;
import com.wangtao.stock.patterns.factory.FileLoggerFactory;
import com.wangtao.stock.patterns.factory.Logger;
import com.wangtao.stock.patterns.factory.LoggerFactory;
import com.wangtao.stock.patterns.factory.abstract2.*;
import com.wangtao.stock.patterns.factory.simple.Chart;
import com.wangtao.stock.patterns.factory.simple.ChartFactory;

public class ClientMain {

    public static void main(String[] args) {
//        simpleFactory();
//        factoryPattern();
//        combination();
//        decorator();
        command();
    }

    /**
     * 简单工厂模式
     */
    public static void simpleFactory() {
        Chart chart;
        chart = ChartFactory.getChart("histogram");
        chart.display();
    }


    /**
     * 工厂模式
     */
    public static void factoryPattern() {
        LoggerFactory factory;
        Logger logger;
        factory = new FileLoggerFactory();
        logger = factory.createLogger();
        logger.writeLog();
    }

    /**
     * 抽象工厂模式
     */
    public static void abstractFactory() {
        SkinFactory skinFactory;
        Button button;
        TextField textField;
        ComboBox comboBox;
        SpringSkinFactory springSkinFactory = new SpringSkinFactory();
        button = springSkinFactory.createButton();
        textField = springSkinFactory.createTextField();
        comboBox = springSkinFactory.createComboBox();
        button.display();
        textField.display();
        comboBox.display();
    }

    /**
     * 适配器模式
     */
    public static void adapterMain() {
        ScoreOperation scoreOperation = new OperationAdapter();
        int[] scores = {84, 76, 50, 69, 90, 91, 88, 96};
        int result[];
        int score;
        System.out.println("成绩排序结果：");
        result = scoreOperation.sort(scores);
        score = scoreOperation.search(result, 90);
    }

    /**
     * 桥接模式
     */
    public static void bridgeMain() {
        Image image = new JPGImage();
        ImageImpl image1 = new WindowsImpl();
        image.setImage(image1);
        image.parseFile("小龙女");
    }

    /**
     * 组合模式
     */
    public static void combination() {
        Folder folder, folder1, folder2;
        folder = new Folder("Sunny的资料");
        folder1 = new Folder("图像文件");
        folder2 = new Folder("文本文件");

        ImageFile image1, image2;
        image1 = new ImageFile("小龙女.jpg");
        image2 = new ImageFile("张无忌.gif");

        TextFile text1, text2;
        text1 = new TextFile("九阴真经.txt");
        text2 = new TextFile("葵花宝典.doc");

        folder2.addImageFile(image2);
        folder2.addImageFile(image1);
        folder1.addTextFile(text1);
        folder1.addTextFile(text2);
        folder.addFolder(folder1);
        folder.addFolder(folder2);

        folder.killVirus();
    }

    /**
     * 装饰器
     */
    public static void decorator() {
        Component component, componentSB;//使用抽象构建定义
        component = new Window();
        componentSB = new ScrollBarDecorator(component);
        componentSB.display();
    }

    /**
     * 命令模式
     */
    public static void command() {
        FBSettingWindow fbSettingWindow = new FBSettingWindow("功能键设置");
        FunctionButton functionButton1, functionButton2;
        functionButton1 = new FunctionButton("功能键1");
        functionButton2 = new FunctionButton("功能键2");
        Command command1, command2;
        command1 = new HelpCommand();
        command2 = new MinimizeCommand();

        functionButton1.setCommand(command1);
        functionButton2.setCommand(command2);
        fbSettingWindow.addFunctionButton(functionButton1);
        fbSettingWindow.addFunctionButton(functionButton2);
        fbSettingWindow.display();
        //调用功能键的业务方法
        functionButton1.onClick();
        functionButton2.onClick();
    }


}
