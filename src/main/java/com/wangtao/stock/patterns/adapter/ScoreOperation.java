package com.wangtao.stock.patterns.adapter;

/**
 * 算法库：目标对象
 */
public interface ScoreOperation {

    int[] sort(int[] array);//成绩排行

    int search(int[] array, int key);//成绩查找

}
