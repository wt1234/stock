package com.wangtao.stock.patterns.adapter;

public class OperationAdapter implements ScoreOperation {

    private QuickSort quickSort;//
    private BinarySearch search;

    public OperationAdapter() {
        quickSort = new QuickSort();
        search = new BinarySearch();
    }


    @Override
    public int[] sort(int[] array) {
        return quickSort.quickSort(array);
    }

    @Override
    public int search(int[] array, int key) {
        return search.binarySearch(array, key);
    }
}
