package com.wangtao.stock.patterns.interpreter;

/**
 * 抽象表达式
 */
public abstract class AbstractNode {

    public abstract String interpret();
}
