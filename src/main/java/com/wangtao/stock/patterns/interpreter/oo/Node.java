package com.wangtao.stock.patterns.interpreter.oo;

public abstract class Node {

    public abstract void interpret(Context context);//声明一个方法用于解释语句

    public abstract void execute();//声明一个方法用于执行标记对应的命令


}
