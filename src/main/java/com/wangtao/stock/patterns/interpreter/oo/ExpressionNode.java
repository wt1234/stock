package com.wangtao.stock.patterns.interpreter.oo;

import java.util.ArrayList;
import java.util.Iterator;

public class ExpressionNode extends Node {

    private ArrayList<Node> list = new ArrayList<>();

    @Override
    public void interpret(Context context) {
        //循环处理context中的标记
        while (true) {
            if (context.currentToken() == null) {
                break;
            } else if (context.currentToken().equals("END")) {
                context.skipToken("END");
                break;
            } else {
                Node commandNode = new CommandNode();
                commandNode.interpret(context);
                list.add(commandNode);
            }
        }
    }

    @Override
    public void execute() {
        Iterator<Node> iterator = list.iterator();
        while (iterator.hasNext()){
            iterator.next().execute();
        }
    }
}
