package com.wangtao.stock.patterns.interpreter.oo;

public class LoopCommandNode extends Node {

    private int number;//循环次数
    private Node commandNode;//循环语句中的表达式


    @Override
    public void interpret(Context context) {
        context.skipToken("LOOP");
        number = context.currentNumber();
        context.nextToken();
        commandNode = new ExpressionNode();
        commandNode.interpret(context);
    }

    @Override
    public void execute() {
        for (int i=0;i<number;i++){
            commandNode.execute();
        }
    }
}
