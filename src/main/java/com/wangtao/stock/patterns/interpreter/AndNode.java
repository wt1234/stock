package com.wangtao.stock.patterns.interpreter;

public class AndNode extends AbstractNode {

    private AbstractNode left;//左侧表达式
    private AbstractNode right;//右侧表达式

    public AndNode(AbstractNode left, AbstractNode right) {
        this.left = left;
        this.right = right;
    }


    @Override
    public String interpret() {
        return left.interpret() + "再"+right.interpret();
    }
}
