package com.wangtao.stock.patterns.interpreter.oo;

import java.util.StringTokenizer;

public class Context {

    private StringTokenizer tokenizer;//切割字符串
    private String currentToken;//当前字符串标记

    public Context(String text) {
        tokenizer = new StringTokenizer(text);
        nextToken();
    }

    public String nextToken() {
        if (tokenizer.hasMoreTokens()) {
            currentToken = tokenizer.nextToken();
        } else {
            currentToken = null;
        }
        return currentToken;
    }

    public String currentToken() {
        return currentToken;
    }

    public void skipToken(String token) {
        if (!token.equals(currentToken)) {
            System.out.println("错误提示:" + currentToken + "解释错误");
        }
        nextToken();
    }

    public int currentNumber() {
        int number = 0;
        number = Integer.parseInt(currentToken);
        return number;
    }
}
