package com.wangtao.stock.patterns.decorator;

/**
 * 抽象组件
 */
public abstract class Component {

    public abstract void display();

}
