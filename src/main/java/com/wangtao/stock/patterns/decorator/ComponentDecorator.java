package com.wangtao.stock.patterns.decorator;


/**
 * 抽象装饰器类
 */
public class ComponentDecorator extends Component {

    private Component component;//维持对抽象构建类型对象的引用

    public ComponentDecorator(Component component) {
        this.component = component;
    }

    @Override
    public void display() {
        component.display();
    }
}
