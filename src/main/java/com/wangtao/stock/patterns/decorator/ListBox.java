package com.wangtao.stock.patterns.decorator;

/**
 * 具体组件
 */
public class ListBox extends Component {
    @Override
    public void display() {
        System.out.println("显示列表框");
    }
}
