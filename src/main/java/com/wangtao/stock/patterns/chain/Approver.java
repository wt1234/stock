package com.wangtao.stock.patterns.chain;

/**
 * 审批者类
 */
public abstract  class Approver {

    protected Approver successor;//定义后继对象

    protected String name;//审批者姓名

    public Approver(String name) {
        this.name = name;
    }

    public abstract void processRequest(PurchaseRequest purchaseRequest);

}
