package com.wangtao.stock.patterns.strategy;

public abstract class Discount {

    public abstract double calculate(double price);
}
