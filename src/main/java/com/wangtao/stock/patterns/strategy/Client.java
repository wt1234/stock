package com.wangtao.stock.patterns.strategy;

public class Client {
    public static void main(String[] args) {
        MovieTicket movieTicket = new MovieTicket();
        double originalPrice = 60;
        double currentPrice;
        movieTicket.setPrice(originalPrice);
        movieTicket.setDiscount(new VIPDiscount());
        currentPrice = movieTicket.getPrice();
        System.out.println(currentPrice);
    }
}
