package com.wangtao.stock.patterns.strategy;

public class VIPDiscount extends Discount {


    @Override
    public double calculate(double price) {
        return price * 0.5;
    }
}
