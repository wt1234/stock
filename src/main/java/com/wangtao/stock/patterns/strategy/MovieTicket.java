package com.wangtao.stock.patterns.strategy;

public class MovieTicket {

     private double price;

     private Discount discount;//维持一个折扣抽象类

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public double getPrice() {
        return discount.calculate(price);
    }
}
