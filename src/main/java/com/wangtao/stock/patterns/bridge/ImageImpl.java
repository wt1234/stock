package com.wangtao.stock.patterns.bridge;

/**
 * 抽象操作系统实现类:具体实现类
 */
public interface ImageImpl {

    void doPaint(Matrix matrix); //显示像素矩阵
}
