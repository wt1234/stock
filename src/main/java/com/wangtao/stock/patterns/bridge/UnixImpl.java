package com.wangtao.stock.patterns.bridge;

public class UnixImpl  implements ImageImpl{

    @Override
    public void doPaint(Matrix matrix) {
        System.out.println("在unix系统显示图像");
    }
}
