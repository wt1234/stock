package com.wangtao.stock.patterns.bridge;

/**
 * 抽象类
 */
public abstract class Abstraction {

    protected Implementor implementor;

    public void setImplementor(Implementor implementor) {
        this.implementor = implementor;
    }

    protected abstract void operation();
}
