package com.wangtao.stock.patterns.bridge;

/**
 * 抽象图像类：抽象类
 */
public abstract class Image {

    protected ImageImpl image;

    public void setImage(ImageImpl image) {
        this.image = image;
    }

    public abstract void parseFile(String fileName);

}
