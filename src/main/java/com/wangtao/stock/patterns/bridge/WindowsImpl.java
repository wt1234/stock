package com.wangtao.stock.patterns.bridge;

public class WindowsImpl  implements ImageImpl{


    @Override
    public void doPaint(Matrix matrix) {
        //调用windows 系统的绘制函数像素矩阵
        System.out.println("在Windows中显示图像");
    }
}
