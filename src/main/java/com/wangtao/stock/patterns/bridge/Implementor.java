package com.wangtao.stock.patterns.bridge;

public interface Implementor {

    public void operatorImpl();
}
