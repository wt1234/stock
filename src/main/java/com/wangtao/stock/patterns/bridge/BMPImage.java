package com.wangtao.stock.patterns.bridge;

public class BMPImage extends Image{
    @Override
    public void parseFile(String fileName) {
        Matrix matrix = new Matrix();
        image.doPaint(matrix);
        System.out.println(fileName+",图像格式为BMP");
    }
}
