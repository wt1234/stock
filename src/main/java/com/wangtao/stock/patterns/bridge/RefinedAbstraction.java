package com.wangtao.stock.patterns.bridge;

public class RefinedAbstraction extends Abstraction{

    @Override
    protected void operation() {
        implementor.operatorImpl();
    }
}
