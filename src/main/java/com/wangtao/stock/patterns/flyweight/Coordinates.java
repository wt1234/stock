package com.wangtao.stock.patterns.flyweight;

import lombok.Getter;
import lombok.Setter;

/**
 * 坐标
 */
@Getter
@Setter
public class Coordinates {

    /**
     * 横坐标
     */
    private Integer x;

    /**
     * 纵坐标
     */
    private Integer y;

    public Coordinates(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }
}
