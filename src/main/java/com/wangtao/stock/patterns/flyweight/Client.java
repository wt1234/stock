package com.wangtao.stock.patterns.flyweight;

/**
 * 享元模式
 */
public class Client {
    public static void main(String[] args) {
        IgoChessman black1, black2, white1, white2;
        //通过享元工厂获取三颗黑子
        black1 = IgoChessmanFactory.getIgoChessman("b");
        black2 = IgoChessmanFactory.getIgoChessman("b");
        System.out.println("判断两个黑子是否相同:" + (black1 == black2));
        //获取两个白子
        white1 = IgoChessmanFactory.getIgoChessman("w");
        white2 = IgoChessmanFactory.getIgoChessman("w");
        //显示棋子
        black1.display(new Coordinates(1,2));
        black1.display(new Coordinates(2,3));
        black1.display(new Coordinates(4,4));
        white1.display(new Coordinates(4,5));
        white2.display(new Coordinates(3,3));
    }
}
