package com.wangtao.stock.patterns.flyweight;

import java.util.concurrent.ConcurrentHashMap;

public class IgoChessmanFactory {

    private static IgoChessmanFactory instance = new IgoChessmanFactory();
    private static ConcurrentHashMap concurrentHashMap;//享元池

    private IgoChessmanFactory() {
        concurrentHashMap = new ConcurrentHashMap();
        IgoChessman black, white;
        black = new BlackIgoChessman();
        concurrentHashMap.put("b", black);
        white = new WhiteIgoChessman();
        concurrentHashMap.put("w", white);
    }

    /**
     * 获取规则工厂
     */
    public static IgoChessmanFactory getInstance() {
        return instance;
    }

    /**
     * 通过key来获取存储在map中的享元对象
     *
     * @param color 入参
     * @return 返回值
     */
    public static IgoChessman getIgoChessman(String color) {
        return (IgoChessman) concurrentHashMap.get(color);
    }
}
