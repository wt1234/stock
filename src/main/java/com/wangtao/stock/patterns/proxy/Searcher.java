package com.wangtao.stock.patterns.proxy;

public interface Searcher {

    String doSearch(String userId, String password);
}
