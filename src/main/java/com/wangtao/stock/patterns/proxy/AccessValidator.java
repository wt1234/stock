package com.wangtao.stock.patterns.proxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AccessValidator {

    //模拟实现登录验证
    public Boolean validate(String userId) {
        log.info("在数据库中验证是否合法" + userId + "是否为合法用户");
        if (userId.equals("杨过")) {
            return true;
        } else {
            return false;
        }
    }
}
