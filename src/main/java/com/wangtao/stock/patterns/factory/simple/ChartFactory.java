package com.wangtao.stock.patterns.factory.simple;

/**
 * 工厂
 */
public class ChartFactory {

    public static Chart getChart(String type) {
        Chart chart = null;
        if ("histogram".equalsIgnoreCase(type)) {
            chart = new HistogramChart();
            System.out.println("初始化设置柱状图");
        } else if ("pie".equalsIgnoreCase(type)) {
            System.out.println("初始化设置饼图");
        } else if ("line".equalsIgnoreCase(type)) {
            System.out.println("初始化设置折线图");
        }
        return chart;
    }
}
