package com.wangtao.stock.patterns.factory.abstract2;

public interface ComboBox {

    void display();
}
