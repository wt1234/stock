package com.wangtao.stock.patterns.factory.simple;

/**
 * 抽象产品对象
 */
public abstract class Chart {

    /**
     * 展示
     */
    public abstract void display();
}
