package com.wangtao.stock.patterns.factory;

public class DatabaseLoggerFactory implements LoggerFactory{


    @Override
    public Logger createLogger() {
        return new DataBaseLogger();
    }
}
