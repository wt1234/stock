package com.wangtao.stock.patterns.factory.abstract2;

/**
 * 抽象工厂
 */
public interface SkinFactory {

    Button createButton();
    TextField createTextField();
    ComboBox createComboBox();
}
