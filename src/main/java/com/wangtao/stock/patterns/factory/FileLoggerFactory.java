package com.wangtao.stock.patterns.factory;

public class FileLoggerFactory implements LoggerFactory{


    @Override
    public Logger createLogger() {
        return new FileLogger();
    }
}
