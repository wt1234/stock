package com.wangtao.stock.patterns.factory;

public interface LoggerFactory {

    Logger createLogger();
}
