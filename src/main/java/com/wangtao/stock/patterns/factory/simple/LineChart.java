package com.wangtao.stock.patterns.factory.simple;

/**
 * 具体产品对象
 */
public class LineChart extends Chart {

    public LineChart() {
        System.out.println("简单工厂模式+创建折线图");
    }

    @Override
    public void display() {
        System.out.println("简单工厂模式+展示折线图");
    }
}
