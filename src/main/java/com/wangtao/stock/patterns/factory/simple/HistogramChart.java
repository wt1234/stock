package com.wangtao.stock.patterns.factory.simple;

/**
 * 具体产品对象
 */
public class HistogramChart extends Chart{

    public HistogramChart() {
        System.out.println("创建柱状图");
    }

    @Override
    public void display() {
        System.out.println("简单工厂模式+展示柱状图");
    }
}
