package com.wangtao.stock.patterns.factory.abstract2;

/**
 * 按钮接口：抽象产品
 */
public interface Button {

    void display();
}
