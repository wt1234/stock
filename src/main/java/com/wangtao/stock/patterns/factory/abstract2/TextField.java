package com.wangtao.stock.patterns.factory.abstract2;

//文本框接口：抽象产品
public interface TextField {

    void display();
}
