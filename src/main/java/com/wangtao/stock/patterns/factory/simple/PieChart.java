package com.wangtao.stock.patterns.factory.simple;


/**
 * 具体产品对象
 */
public class PieChart extends Chart{

    public PieChart() {
        System.out.println("简单工厂模式+创建饼图");
    }

    @Override
    public void display() {
        System.out.println("简单工厂模式+展示饼图");
    }
}
