package com.wangtao.stock.patterns.factory;

public interface Logger {

    void writeLog();
}
