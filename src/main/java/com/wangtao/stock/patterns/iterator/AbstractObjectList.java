package com.wangtao.stock.patterns.iterator;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractObjectList<T> {

    protected List<T> objects = new ArrayList<>();

    public AbstractObjectList(List<T> objects) {
        this.objects = objects;
    }

    public void addObject(T obj) {
        this.objects.add(obj);
    }

    public void removeObject(T obj) {
        this.objects.remove(obj);
    }

    public List<T> getObjects() {
        return this.objects;
    }

    public abstract AbstractIterator createIterator();
}
