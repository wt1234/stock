package com.wangtao.stock.patterns.iterator;

public interface Iterator {

    void first();

    void next();

    void hasNext();

    void currentItem();


}
