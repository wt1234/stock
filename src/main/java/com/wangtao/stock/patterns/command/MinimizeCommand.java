package com.wangtao.stock.patterns.command;

/**
 * 具体命令类
 */
public class MinimizeCommand  extends Command{

    private WindowHandler windowHandler;

    public MinimizeCommand() {
        windowHandler = new WindowHandler();
    }

    @Override
    public void execute() {
        windowHandler.minimize();
    }
}
