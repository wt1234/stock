package com.wangtao.stock.patterns.command;

import java.util.ArrayList;

public class FBSettingWindow {

    private String title;//标题

    private ArrayList<FunctionButton> functionButtons = new ArrayList<>();

    public FBSettingWindow(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addFunctionButton(FunctionButton functionButton) {
        functionButtons.add(functionButton);
    }

    public void removeFunctionButton(FunctionButton functionButton) {
        functionButtons.remove(functionButton);
    }

    public void display() {
        System.out.println("显示窗口：" + this.title);
        System.out.println("显示功能键:");
        for (FunctionButton object : functionButtons) {
            System.out.println(object.getName());
        }
        System.out.println("----------------");
    }
}
