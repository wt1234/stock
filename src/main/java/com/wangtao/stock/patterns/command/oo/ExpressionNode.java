package com.wangtao.stock.patterns.command.oo;

import java.util.ArrayList;

public class ExpressionNode extends Node {

    private ArrayList<Node> list = new ArrayList<>();

    @Override
    public void interpret(Context context) {
        while (true) {
            //如果已经没有任何标记，则退出解释
            if (context.currentToken() == null) {
                break;
            }
            //如果标记为END,则不解释END并结束本次解释过程，可以继续之后的解释
            else if ("END".equals(context.currentToken())) {
                context.skipToken("END");
                break;
            }
            else {
//                Node commandNode = new C
            }
        }
    }

    @Override
    public void execute() {

    }
}
