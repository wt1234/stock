package com.wangtao.stock.patterns.command.oo;

public abstract class Node {

    public abstract void interpret(Context context);

    public abstract void execute();
}



