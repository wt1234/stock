package com.wangtao.stock.patterns.command.oo;

import java.util.StringTokenizer;

public class Context {

    private StringTokenizer stringTokenizer;
    private String currentToken;

    public Context(String text) {
        stringTokenizer = new StringTokenizer(text);

    }

    public String nextToken() {
        if (stringTokenizer.hasMoreTokens()) {
            currentToken = stringTokenizer.nextToken();
        } else {
            currentToken = null;
        }
        return currentToken;
    }

    /**
     * 返回当前的标记
     */
    public String currentToken() {
        return currentToken;
    }

    public void skipToken(String token) {
        if (!token.equals(currentToken)) {
            System.out.println("错误提示:" + currentToken + "解释错误");
        }
        nextToken();
    }

    public int currentNumber(){
        int number =0;
        number = Integer.parseInt(currentToken);
        return number;
    }




}
