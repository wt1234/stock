package com.wangtao.stock.patterns.command;

/**
 * 具体命令类
 */
public class HelpCommand  extends Command{

    private HelpHandler helpCommand; //维持对请求接受者的引用

    public HelpCommand(){
        helpCommand = new HelpHandler();
    }

    @Override
    public void execute() {
        helpCommand.display();
    }
}
