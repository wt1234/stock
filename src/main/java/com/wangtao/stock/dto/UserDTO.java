package com.wangtao.stock.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserDTO {

    /**
     * 用户名称
     */
    @NotNull(message = "用户名不能为空", groups = AddUserDTO.class)
    private String usrName;

    /**
     * 地址
     */
    @NotNull(message = "地址不能为空", groups = UpdateUserDTO.class)
    private String address;

    /**
     * 邮箱
     */
    private String email;


    /**
     * 添加分组
     */
    public interface AddUserDTO {
    }

    /**
     * 修改分组
     */
    public interface UpdateUserDTO {
    }

}
