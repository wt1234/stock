package com.wangtao.stock.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class ArticleDTO {

    /**
     * 文章编号
     */
    @NotNull(message = "文章id不能为空")
    @Min(value = 1, message = "文章id不能为负数")
    private Integer id;

    @NotBlank(message = "文章内容不能为空")
    private String content;

    @NotBlank(message = "作者id不为空")
    private String authorId;

    @Future(message = "提交时间不能为过去时间")
    private Date submitTime;

    //修改文章的分组
    public interface UpdateArticleDTO {
    }

    //添加文章的分组
    public interface AddArticleDTO {
    }
}
