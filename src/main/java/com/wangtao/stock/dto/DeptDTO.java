package com.wangtao.stock.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import java.util.Date;

@Getter
@Setter
public class DeptDTO {

    /**
     * 部门编号
     */
    private String deptNum;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 所属公司
     */
    private String company;

    /**
     * 创建时间
     */
    private Date createTime;

    @Valid
    private UserDTO userDTO;




}
