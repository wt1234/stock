package com.wangtao.stock.service.impl;

import com.wangtao.stock.annotation.SwitchSource;
import com.wangtao.stock.dao.HisDeptInfoMapper;
import com.wangtao.stock.entity.DeptInfo;
import com.wangtao.stock.service.HisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HisServiceImpl implements HisService {

    @Autowired
    private HisDeptInfoMapper hisDeptInfoMapper;

    @SwitchSource
    @Override
    public List<DeptInfo> list() {
        return hisDeptInfoMapper.queryDeptInfoList();
    }
}
