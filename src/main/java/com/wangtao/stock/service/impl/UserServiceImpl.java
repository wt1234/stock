package com.wangtao.stock.service.impl;

import com.wangtao.stock.annotation.SwitchSource;
import com.wangtao.stock.dao.UserInfoMapper;
import com.wangtao.stock.entity.User;
import com.wangtao.stock.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserInfoMapper mapper;

    @SwitchSource
    @Override
    public List<User> query() {
        mapper.selectUserInfo();
        return null;
    }

    @SwitchSource
    public void save() {
        User user = new User();
        user.setUserName("aaaa");
        user.setCreateTime(new Date());
        user.setPassword("11111");
        mapper.insertUser(user);
    }
}
