package com.wangtao.stock.service;

import com.wangtao.stock.entity.User;

import java.util.List;

public interface UserService {

    List<User> query();

    void save();
}
