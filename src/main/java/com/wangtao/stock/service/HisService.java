package com.wangtao.stock.service;

import com.wangtao.stock.entity.DeptInfo;

import java.util.List;

public interface HisService {

    List<DeptInfo> list();
}
